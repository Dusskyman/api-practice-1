import 'dart:convert';
import 'dart:math';

import 'package:api_practice_1/models/user.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Future<User> getRandomUser() async {
    return User.dto(
      await http
          .get(
              'https://randomapi.com/api/6de6abfedb24f889e0b5f675edc50deb?fmt=raw&sole')
          .then(
        (value) {
          var userList = json.decode(value.body) as List;
          return userList[Random().nextInt(userList.length)];
        },
      ),
    );
    // print(user.name);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: FutureBuilder(
          future: getRandomUser(),
          builder: (context, AsyncSnapshot<User> snapshot) {
            if (snapshot.hasData) {
              var data = snapshot.data;
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Name: ${data.name}",
                    style: TextStyle(fontSize: 30),
                  ),
                  Text(
                    "Surname: ${data.surname}",
                    style: TextStyle(fontSize: 30),
                  ),
                  Text(
                    "Email: ${data.email}",
                    style: TextStyle(fontSize: 20),
                  ),
                  Text(
                    "Address: ${data.adress}",
                    style: TextStyle(fontSize: 20),
                  ),
                  TextButton(
                    onPressed: () {
                      setState(() {
                        // getRandomUser();
                      });
                    },
                    child: Text('Get new user'),
                  ),
                ],
              );
            }
            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }
}
