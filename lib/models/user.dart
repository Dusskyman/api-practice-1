class User {
  final String name;
  final String surname;
  final String email;
  final String adress;

  User({
    this.name,
    this.surname,
    this.email,
    this.adress,
  });

  factory User.dto(Map<String, dynamic> user) => User(
        name: user['first'],
        surname: user['last'],
        email: user['email'],
        adress: user['address'],
      );
}
